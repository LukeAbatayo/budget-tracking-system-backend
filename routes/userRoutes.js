const router = require('express').Router();
const { verifyUser } = require('./../auth.js');
const { register, login, getUserInfo } = require('./../controllers/userController')

router.post('/', register);
router.post('/login', login);
router.get('/', verifyUser, getUserInfo);

module.exports = router;