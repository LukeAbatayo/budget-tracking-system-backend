const mongoose = require('mongoose');

const IncomeSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        reqiure: true
    },
    incomeCategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "IncomeCategory",
        reqiure: true
    },
    amount: {
        type: Number,
        require: true
    },
});

module.exports = mongoose.model('Income', IncomeSchema);