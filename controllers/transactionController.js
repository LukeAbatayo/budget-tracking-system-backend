const Income = require('./../models/Income');
const Expense = require('./../models/Expense');

// Income
module.exports.getAllIncome = (req, res) => {

    Income.find({ userId: req.user.id })
    .populate('incomeCategoryId')
    .then(UserIncomeList => {
        return res.send(UserIncomeList)
    })
    .catch(err => res.send(err));
}

module.exports.addIncome = (req, res) => {
    
    Income.create({
        userId: req.user.id,
        incomeCategoryId: req.params.id,
        amount: req.body.amount
    })
    .then(income => {
        res.send(income);
    })
    .catch(err => res.send(err));
}

module.exports.updateIncome = (req, res, next) => {

    Income.findByIdAndUpdate(req.params.incomeId, req.body, {new: true})
    .then(income => {
        res.send(income);
    })
    .catch(next);
}

module.exports.deleteIncome = (req, res, next) => {

    Income.findByIdAndDelete(req.params.incomeId)
    .then(income => {
        res.send(income);
    })
    .catch(next);
}

// Expense
module.exports.getAllExpense = (req, res) => {

    Expense.find({ userId: req.user.id })
    .populate('expenseCategoryId')
    .then(UserExpenseList => {
        return res.send(UserExpenseList)
    })
    .catch(err => res.send(err));
}

module.exports.addExpense = (req, res) => {

    Expense.create({
        userId: req.user.id,
        expenseCategoryId: req.params.id,
        amount: req.body.amount
    })
    .then(expense => {
        res.send(expense);
    })
    .catch(err => res.send(err));
}

module.exports.updateExpense = (req, res, next) => {
    
    Expense.findByIdAndUpdate(req.params.expenseId, req.body, {new: true})
    .then(expense => {
        res.send(expense);
    })
    .catch(next);

}

module.exports.deleteExpense = (req, res, next) => {

    Expense.findByIdAndDelete(req.params.expenseId)
    .then(expense => {
        res.send(expense);
    })
    .catch(next);
}