const IncomeCategory = require('./../models/IncomeCategory');
const ExpenseCategory = require('./../models/ExpenseCategory');
const Income = require('./../models/Income');
const Expense = require('./../models/Expense');

// Income Category
module.exports.getAllCategory = (req, res) => {

    IncomeCategory.find({ userId: req.user.id })
    .then(category => {
        return res.send(category)
    })
    .catch(err => res.send(err));
}

module.exports.addCategory = (req, res) => {

    IncomeCategory.create({
        userId: req.user.id,
        category: req.body.category,
    })
    .then(category => {
        res.send(category);
    })
    .catch(err => res.send(err));
}

module.exports.updateCategory = (req, res, next) => {

    IncomeCategory.findByIdAndUpdate(req.params.id, req.body, {new: true})
    .then(category => {
        res.send(category);
    })
    .catch(next);
}

module.exports.deleteCategory = (req, res, next) => {

    IncomeCategory.findByIdAndDelete(req.params.id)
    .then(category => {

        Income.remove({incomeCategoryId: req.params.id})
        .then(res => console.log(typeof(req.params.id)))

        res.send(category);
    })
    .catch(next);
}

// Expense Category
module.exports.getAllExpenseCategory = (req, res) => {

    ExpenseCategory.find({ userId: req.user.id })
    .then(category => {
        return res.send(category)
    })
    .catch(err => res.send(err));
}

module.exports.addExpenseCategory = (req, res) => {

    ExpenseCategory.create({
        userId: req.user.id,
        category: req.body.category,
    })
    .then(category => {
        res.send(category);
    })
    .catch(err => res.send(err));
}

module.exports.updateExpenseCategory = (req, res, next) => {

    ExpenseCategory.findByIdAndUpdate(req.params.id, req.body, {new: true})
    .then(category => {
        res.send(category);
    })
    .catch(next);
}

module.exports.deleteExpenseCategory = (req, res, next) => {

    ExpenseCategory.findByIdAndDelete(req.params.id)
    .then(category => {
        
        Expense.remove({expenseCategoryId: req.params.id})
        .then(res => console.log(typeof(req.params.id)))

        res.send(category);
    })
    .catch(next);
}