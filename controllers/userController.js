const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('./../models/User');
const saltRounds = parseInt(process.env.SALT_ROUNDS);
const privateKey = process.env.PRIVATE_KEY;

module.exports.register = (req, res, next) => {

    // Validate password - min 8 characters
    let { password, confirmPassword } = req.body;
    if (password.length < 8 || password !== confirmPassword) {
        throw new Error("Password must be atleast 8 characters and must be matched confirm password");
    }

    // hash password
    bcrypt.hash(req.body.password, saltRounds)
    .then(hash => {
        req.body.password = hash;

        return User.create(req.body);
    })
    .then(user => {
        user.password = undefined;

        res.send(user);
    })
    .catch(next);
}

module.exports.login = (req, res, next) => {

    User.findOne({email: req.body.email})
    .then(user => {
        if (user) {
            req.user = user;

            return user;
        } else {
            res.status(401);

            next(new Error("Invalid credentials"));
        }
    })
    .then(user => {
        return bcrypt.compare(req.body.password, user.password);
    })
    .then(isMatchPassword => {
        if (isMatchPassword) {
            return jwt.sign({userId: req.user._id}, privateKey);
        } else {
            res.status(401);

            next(new Error("Invalid credentials"));
        }
    })
    .then(token => {
        res.send({token});
    })
    .catch(next);
}

module.exports.getUserInfo = (req, res, next) => {
    res.send(req.user)
}